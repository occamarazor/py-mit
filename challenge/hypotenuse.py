import math


def get_hypotenuse(a, b):
    return math.sqrt(a**2 + b**2)


print(get_hypotenuse(3, 5))
