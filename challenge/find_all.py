def find_all(word_list, char_str):
    """assumes: word_list is a list of words in lowercase.
                char_str is a str of lowercase letters.
                No letter occurs in char_str more than once
       returns: a list of all the words in word_list that contain
                each of the letters in char_str exactly once and no
                letters not in char_str."""
    new_list = []
    char_str = ''.join(sorted(char_str))
    for word in word_list:
        if char_str == ''.join(sorted(word)):
            new_list.append(word)
    return new_list


print(find_all(['pot', 'stop', 'toast', 'spot', 'toss', 'post'], 'pots'))
print(find_all(['', 'loop', 'polo', 'bloop', 'poll', 'pool'], 'oopl'))
