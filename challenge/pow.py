def pow(num, exp):
    """
    Raises a number to an exponent power
    :param num: number
    :type num: float
    :param exp: exponent
    :type exp: float
    :return: num to exp power
    :rtype: float
    """
    return num**exp


x = float(input('Enter a number: '))
y = float(input('Enter an exponent: '))
print(pow(x, y))
