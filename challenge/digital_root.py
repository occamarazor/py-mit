from functools import reduce


def digital_root(n):
    if len(str(n)) > 1:
        return digital_root(reduce(lambda x, y: int(x) + int(y), list(str(n))))
    else:
        return n


print(digital_root(942))
