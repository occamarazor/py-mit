def hanoi(n, s, t, b):
    assert n > 0
    if n == 1:
        print('move', s, 'to', t)
    else:
        hanoi(n - 1, s, b, t)
        hanoi(1, s, t, b)
        hanoi(n - 1, b, t, s)


print(hanoi(4, 'Source', 'Target', 'Buffer'))
