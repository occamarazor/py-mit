def get_divisors_list(x):
    divisors = ()
    for i in range(1, x):
        if x % i == 0:
            divisors = divisors + (i,)
    return divisors


print(get_divisors_list(1000))
