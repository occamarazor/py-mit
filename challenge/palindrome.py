# Recursive solution

# def is_palindrome(string):
#     string = ' '.join(string.split())
#     check = True
#
#     if len(string) > 1:
#         first_i = 0
#         last_i = len(string) - 1
#         check = string[first_i] == string[last_i]
#         new_string = string[first_i + 1:last_i]
#         if check is True:
#             return is_palindrome(new_string)
#     return check


def is_palindrome(string):
    string = ' '.join(string.split())
    s = string[::-1]
    return string == s


print('res:', is_palindrome(' omen   fof nemo'))
