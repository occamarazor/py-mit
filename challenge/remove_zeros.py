def remove_zeros(array):
    array.sort(key=lambda i: i is 0 or str(i) == '0.0')
    return array


print(remove_zeros([0, 1, None, 2, False, 1, 0]))
print(remove_zeros([9, 0.0, 9, 1, 2, 1, 1, 0.0, 3, 1, 9, 9, 0, 0, 0, 0, 0, 0, 0, 0]))
