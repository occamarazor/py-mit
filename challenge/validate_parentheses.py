def validate_parentheses(string, pair_sum=0):
    if pair_sum >= 0:
        for c in string:
            i = string.find(c)
            if c == '(':
                pair_sum += 1
                return validate_parentheses(string[i+1:], pair_sum)
            elif c == ')':
                pair_sum += -1
                return validate_parentheses(string[i+1:], pair_sum)
            else:
                return validate_parentheses(string[i+1:], pair_sum)
        return pair_sum == 0
    else:
        return False

# O(n)
# def validate_parentheses(string):
#     cnt = 0
#     for c in string:
#         if c == '(': cnt += 1
#         if c == ')': cnt -= 1
#         if cnt < 0: return False
#     return True if cnt == 0 else False


print(validate_parentheses("  ("))
print(validate_parentheses(")test"))
print(validate_parentheses(""))
print(validate_parentheses("hi())("))
print(validate_parentheses("hi(hi)()"))
