def add_vectors(v1, v2):
    """assumes v1 and v2 are lists of ints.
       Returns a list containing the pointwise sum of
       the elements in v1 and v2.  For example,
       addVectors([4,5], [1,2,3]) returns [5,7,3],and
       addVectors([], []) returns []. Does not modify inputs."""
    if len(v1) > len(v2):
        result = v1[:]
        other = v2
    else:
        result = v2[:]
        other = v1
    for i in range(len(other)):
        result[i] += other[i]
    print(result is v1)
    print(result is v2)
    return result


print(add_vectors([1, 3, 4], [5, 2]))
print(add_vectors([3, 2], [4, 5, 1]))
print(add_vectors([], []))
