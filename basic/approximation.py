# The square root of an int
x = int(input('Enter an integer: '))
# Epsilon
# ans**2 == x +- eps
eps = 0.01
guesses = 0
ans = 0

while abs(ans**2 - x) >= eps and ans <= x:
    ans += 0.0001
    guesses += 1
print('Guesses:', guesses)

if abs(ans**2 - x) >= eps:
    print('Failed on square root of', x)
else:
    print(ans, 'is close to square root of', x)
