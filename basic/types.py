# 'str'
print(type(''))
# 'int'
print(type(3))
# 'float'
print(type(3.0))
# 'bool'
print(type(True))
# 'NoneType'
print(type(None))
# 'tuple'
print(type((1, 2, 3)))
