def exponentiate(b, n):
    res = 1
    if n > 0:
        res = b * exponentiate(b, n - 1)
    return res


print(exponentiate(2, 3))
