# heterogeneous DS
x = (1, 2, 'abc')

# zero based index
print(x[1])
# from end
print(x[-2])
# IndexError
# print(x[3])

# homogeneous DS
y = (1, 2, 3)

# single elem tuple
z = (1,)

# tuples can contain tuples
a = ((1, 2), 3, 4)
print(a[0][0])

# slicing
print(a[0:1])
print(a[:1])
print(a[1:])
print(a[:-1])

# joining
print(x + y)

# iterating
for num in y:
    print(num)
