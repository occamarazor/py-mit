def within_epsilon(x, y, eps):
    """
    :param x: interval endpoint 1
    :type x: float
    :param y: interval endpoint 2
    :type y: float
    :param eps: error value, eps > 0
    :type eps: float
    :return: True if x is within eps
    :rtype: bool
    """
    return abs(x - y) <= eps


print(within_epsilon(2, 5, 1))
