x = True

print('start')
try:
    x[0] is True
    print('no error occurred')
except NameError:
    print('NameError handled')
except Exception as e:
    print('Any error handled:', e)
print('end')

raise TypeError('Raise a TypeError')
