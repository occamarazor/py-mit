# 'ab'
print('a' + 'b')
# 'a3'
print('a' + str(3))
# 4
print(1 + int('3'))
# 4
print(1 + int(3.1))
