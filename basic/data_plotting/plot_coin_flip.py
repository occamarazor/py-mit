import random
import pylab


def plot_coin_flip(min_exp, max_exp):
    ratios = []
    diffs = []
    x_axis = []

    for exp in range(min_exp, max_exp + 1):
        x_axis.append(2**exp)
    for flips in x_axis:
        heads = 0
        for n in range(flips):
            if random.random() < 0.5:
                heads += 1
        tails = flips - heads
        ratios.append(heads/float(tails))
        diffs.append(abs(heads - tails))

    pylab.figure(1)
    pylab.title('Diff between heads & tails')
    pylab.xlabel('Number of flips')
    pylab.ylabel('Abs (Heads - Tails)')
    pylab.plot(x_axis, diffs, 'bo')
    pylab.semilogx()
    pylab.semilogy()

    pylab.figure(2)
    pylab.title('Heads/Tails ratios')
    pylab.xlabel('Number of flips')
    pylab.ylabel('Heads/Tails')
    pylab.plot(x_axis, ratios, 'bo')
    pylab.semilogx()

    pylab.show()


plot_coin_flip(4, 20)
