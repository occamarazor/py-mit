import pylab


pylab.figure(1)
pylab.plot([1, 2, 3, 4], [1, 2, 3, 4])
pylab.savefig('plot_data_first')

pylab.figure(2)
pylab.plot([1, 2, 3, 4], [1, 4, 9, 16])
pylab.savefig('plot_data_second')

pylab.figure(1)
pylab.plot([1, 2, 3, 4])  # x = range(0, 4)

pylab.show()


