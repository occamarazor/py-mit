# The square root of a float
x = float(input('Enter a positive float: '))
high = max(x, 1)
low = 0
eps = 0.01
guesses = 0
ans = (high + low) / 2

while abs(ans**2 - x) >= eps and ans <= x:
    print('low:', low, '\thigh:', high, '\tans:', ans)
    guesses += 1
    if ans**2 < x:
        low = ans
    else:
        high = ans
    ans = (high + low) / 2
print('Guesses:', guesses)
print(ans, 'is close to square root of', x)
