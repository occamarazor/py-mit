# Probability of an exact sequence of 10 rolls is 1/6**10,
# 6 - die possibilities
# 10 - number of rolls

# + Probability of getting 2 sixes in 24 rolls
import random


def roll_die():
    """ returns a random between 1 & 6 """
    return random.choice([1, 2, 3, 4, 5, 6])


def test_roll(n=10):
    result = ''
    for i in range(n):
        result = result + str(roll_die()) + ' '
    return result


def check_pascal(trials=100000):
    yes = 0
    for i in range(trials):
        for j in range(24):
            d1 = roll_die()
            d2 = roll_die()
            if d1 == 6 and d2 == 6:
                yes += 1
                break
    return 'Possibility of getting 2 sixes in 24 rolls = ' + str(yes/trials)


# print(test_roll())
print(check_pascal())
