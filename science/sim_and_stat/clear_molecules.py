# Molecules clearing exponential decay
import random
import pylab


# Analytic model
def clear_molecules(molecules, clear_prob, steps):
    num_remaining = [molecules]
    for t in range(steps):
        num_remaining.append(molecules * ((1 - clear_prob) ** t))
    pylab.plot(num_remaining, label='Analytic')


# clear_molecules(1000, 0.01, 500)
# pylab.semilogy()
# pylab.show()


# Simulation model
def clear_sim(molecules, clear_prob, steps):
    num_remaining = [molecules]
    for t in range(steps):
        num_left = num_remaining[-1]
        for m in range(num_remaining[-1]):
            if random.random() <= clear_prob:
                num_left -= 1
        # Every 100 steps a molecule could clone itself!
        if t != 0 and t % 100 == 0:
            num_left += num_left
        num_remaining.append(num_left)
    pylab.plot(num_remaining, 'r', label='Simulation')


clear_molecules(1000, 0.01, 500)
clear_sim(1000, 0.01, 500)
pylab.show()

