# Random walks
import random


class Location(object):
    """ drunk's coordinates in the field """
    def __init__(self, x, y):
        """ x, y are floats """
        self.x = x
        self.y = y

    def move(self, delta_x, delta_y):
        """ delta_x, delta_y are floats """
        return Location(self.x + delta_x, self.y + delta_y)

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def dist_from(self, other):
        x_dist = self.x - other.x
        y_dist = self.y - other.y
        return (x_dist**2 + y_dist**2) ** 0.5

    def __str__(self):
        return 'x' + str(self.x) + ', ' + str(self.y) + 'y'


class Field(object):
    """ collection of drunks
        maps drunks to locations """
    def __init__(self):
        self.drunks = {}

    def add_drunk(self, drunk, loc):
        if drunk in self.drunks:
            raise ValueError('Duplicate drunk!')
        else:
            self.drunks[drunk] = loc

    def move_drunk(self, drunk):
        if drunk not in self.drunks:
            raise ValueError('Drunk not in Field!')
        x_dist, y_dist = drunk.take_step()
        self.drunks[drunk] = self.drunks[drunk].move(x_dist, y_dist)

    def get_loc(self, drunk):
        if drunk not in self.drunks:
            raise ValueError('Drunk not in Field!')
        return self.drunks[drunk]


class Drunk(object):
    def __init__(self, name):
        self.name = name

    def take_step(self):
        step_choices = [(0, 1), (0, -1), (1, 0), (-1, 0)]
        return random.choice(step_choices)

    def __str__(self):
        return 'This drunk\'s name is ' + self.name


def walk(f, d, steps):
    start = f.get_loc(d)
    for s in range(steps):
        f.move_drunk(d)
    return start.dist_from(f.get_loc(d))


def sim_walks(steps, trials):
    homer = Drunk('Homer')
    origin = Location(0, 0)
    distances = []
    for t in range(trials):
        f = Field()
        f.add_drunk(homer, origin)
        distances.append(walk(f, homer, steps))
    return distances


def drunk_test(trials):
    for steps in [10, 100, 1000, 10000, 100000]:
        distances = sim_walks(steps, trials)
        print('Random walk of ' + str(steps) + ' steps')
        print('  Mean =', sum(distances) / len(distances))
        print('  Max =', max(distances))
        print('  Min =', min(distances))


drunk_test(10)
