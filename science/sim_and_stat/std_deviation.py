import random
import pylab


def std_dev(values):
    mean = sum(values) / float(len(values))
    total = 0
    for v in values:
        total += (v - mean) ** 2
    return (total / len(values)) ** 0.5


def plot_coin_flip(min_exp, max_exp, trials):
    mean_ratios = []
    mean_diffs = []
    ratios_std_devs = []
    diffs_std_devs = []
    x_axis = []

    for exp in range(min_exp, max_exp + 1):
        x_axis.append(2 ** exp)

    for flips in x_axis:
        ratios = []
        diffs = []

        for t in range(trials):
            heads = 0
            for f in range(flips):
                if random.random() > 0.5:
                    heads += 1
            tails = flips - heads
            ratios.append(heads / float(tails))
            diffs.append(abs(heads - tails))

        mean_ratios.append(sum(ratios) / trials)
        mean_diffs.append(sum(diffs) / trials)
        ratios_std_devs.append(std_dev(ratios))
        diffs_std_devs.append(std_dev(diffs))

    pylab.plot(x_axis, mean_ratios, 'bo')
    pylab.title('Mean Heads/Tails Ratios ({} Trials)'.format(trials))
    pylab.xlabel('Number of Flips')
    pylab.ylabel('Mean Heads/Tails')
    pylab.semilogx()
    pylab.figure()

    pylab.plot(x_axis, ratios_std_devs, 'bo')
    pylab.title('SD Heads/Tails Ratios ({} Trials)'.format(trials))
    pylab.xlabel('Number of Flips')
    pylab.ylabel('Standard Deviation')
    pylab.semilogx()
    pylab.semilogy()
    pylab.figure()

    pylab.plot(x_axis, mean_diffs, 'bo')
    pylab.title('Mean abs(Heads - Tails) ({} Trials)'.format(trials))
    pylab.xlabel('Number of Flips')
    pylab.ylabel('Mean abs(Heads - Tails)')
    pylab.semilogx()
    pylab.semilogy()
    pylab.figure()

    pylab.plot(x_axis, diffs_std_devs, 'bo')
    pylab.title('SD abs(Heads - Tails) ({} Trials)'.format(trials))
    pylab.xlabel('Number of Flips')
    pylab.ylabel('Standard Deviation')
    pylab.semilogx()
    pylab.semilogy()


# plot_coin_flip(4, 20, 20)
# pylab.show()

def flip(flips):
    heads = 0
    for f in range(flips):
        if random.random() < 0.5:
            heads += 1
    return heads / flips


def flip_sim(flips_per_trial, trials):
    heads_fraction = []
    for t in range(trials):
        heads_fraction.append(flip(flips_per_trial))
    return heads_fraction


def label_plot(nf, nt, mean, sd):
    """nt = number of trials per experiment
       nf = number of flips
       sd = standard deviation """
    pylab.title('{0} trials of {1} flips each'.format(nt, nf))
    pylab.xlabel('Fraction of Heads')
    pylab.ylabel('Number of Trials')
    x_min, x_max = pylab.xlim()
    y_min, y_max = pylab.ylim()
    pylab.text(
        x_min + (x_max - x_min) * 0.02, (y_max - y_min) / 2,
        'Mean =  {0},\nSD = {1}'.format(round(mean, 6), round(sd, 6))
    )


def make_plots(nf1, nf2, nt):
    """nt = number of trials per experiment
       nf1 = number of flips 1st experiment
       nf2 = number of flips 2nd experiment"""
    heads_fraction_1 = flip_sim(nf1, nt)
    mean1 = sum(heads_fraction_1) / float(len(heads_fraction_1))
    sd1 = std_dev(heads_fraction_1)
    pylab.hist(heads_fraction_1, bins=20)
    x_min, x_max = pylab.xlim()
    y_min, y_max = pylab.ylim()
    label_plot(nf1, nt, mean1, sd1)
    pylab.figure()

    heads_fraction_2 = flip_sim(nf2, nt)
    mean2 = sum(heads_fraction_2) / float(len(heads_fraction_2))
    sd2 = std_dev(heads_fraction_2)
    pylab.hist(heads_fraction_2, bins=20)
    pylab.xlim(x_min, x_max)
    y_min, y_max = pylab.ylim()
    label_plot(nf2, nt, mean2, sd2)


# L = [1, 2, 3, 3, 3, 4]
# pylab.hist(L, bins=6)
# pylab.show()

# make_plots(100, 1000, 100000)
# pylab.show()

def poll(n, p):
    votes = 0
    for i in range(n):
        if random.random() < p / 100:
            votes += 1
    return votes


def test_err(n=1000, p=46, trials=1000):
    results = []
    for t in range(trials):
        results.append(poll(n, p))
    print('STD = {}%'.format((std_dev(results) / n) * 100))
    results = pylab.array(results) / n
    pylab.hist(results)
    pylab.xlabel('Fraction of Votes')
    pylab.ylabel('Number of Polls')


test_err()
pylab.show()
