import random


def flip_coin(flips):
    heads = 0
    for i in range(flips):
        if random.random() < 0.5:
            heads += 1
    return heads/float(flips)


print(flip_coin(9))
