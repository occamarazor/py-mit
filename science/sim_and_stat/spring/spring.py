# Spring displacement (Hooke's law)
import pylab


def get_data(file_name):
    data_file = open(file_name)
    distances = []
    masses = []
    data_file.readline()  # discard header

    for line in data_file:
        d, m = line.split()
        distances.append(float(d))
        masses.append(float(m))

    data_file.close()
    return masses, distances


def plot_data(file_name):
    x_vals, y_vals = get_data(file_name)
    x_vals = pylab.array(x_vals)
    y_vals = pylab.array(y_vals)
    # x_vals = pylab.array(x_vals[:-6])
    # y_vals = pylab.array(y_vals[:-6])
    x_vals = x_vals * 9.81  # acceleration due to gravity
    pylab.plot(x_vals, y_vals, 'bo', label='Measured displacements')
    pylab.title('Measured Displacement of Spring')
    pylab.xlabel('Force (Newtons)')
    pylab.ylabel('Distance (meters)')
    return x_vals, y_vals


# plot_data('spring.txt')
# pylab.show()


def fit_data(file_name):
    x_vals, y_vals = plot_data(file_name)

    a, b = pylab.polyfit(x_vals, y_vals, 1)  # fit a line
    est_y_vals = a * x_vals + b
    k = 1 / a
    pylab.plot(x_vals, est_y_vals, label='Linear fit, k = {}'.format(round(k, 5)))
    pylab.legend(loc='best')
    return x_vals, y_vals


# fit_data('spring.txt')
# pylab.show()


def fit_data_1(file_name):
    x_vals, y_vals = fit_data(file_name)

    a, b, c, d = pylab.polyfit(x_vals, y_vals, 3)
    est_y_vals = a * (x_vals ** 3) + b * x_vals ** 2 + c * x_vals + d
    pylab.plot(x_vals, est_y_vals, label='Cubic fit')
    pylab.legend(loc='best')
    return x_vals, y_vals


# fit_data_1('spring.txt')
# pylab.show()


def fit_data_2(file_name):
    x_vals, y_vals = get_data(file_name)
    extended_x = pylab.array(x_vals + [1.5])
    x_vals = pylab.array(x_vals)
    y_vals = pylab.array(y_vals)
    x_vals *= 9.81
    extended_x *= 9.81

    pylab.plot(x_vals, y_vals, 'bo', label='Measured displacements')
    pylab.title('Measured Displacement of Spring')
    pylab.xlabel('Force (Newtons)')
    pylab.ylabel('Distance (meters)')

    a, b = pylab.polyfit(x_vals, y_vals, 1)
    extended_y = a * pylab.array(extended_x) + b
    pylab.plot(extended_x, extended_y, label='Linear fit')
    a, b, c, d = pylab.polyfit(x_vals, y_vals, 3)
    extended_y = a * (extended_x ** 3) + b * extended_x ** 2 + c * extended_x + d
    pylab.plot(extended_x, extended_y, label='Cubic fit')
    pylab.legend(loc='best')


fit_data_2('spring.txt')
pylab.show()
