# Bow and arrow spring displacement (Hooke's law)
import pylab


def get_trajectory_data(file_name):
    data_file = open(file_name)
    distances = []
    heights1, heights2, heights3, heights4 = [], [], [], []
    data_file.readline()  # discard header

    for line in data_file:
        d, h1, h2, h3, h4 = line.split()
        distances.append(float(d))
        heights1.append(float(h1))
        heights2.append(float(h2))
        heights3.append(float(h3))
        heights4.append(float(h4))
    data_file.close()

    return distances, [heights1, heights2, heights3, heights4]


def try_fits(file_name):
    distances, heights = get_trajectory_data(file_name)
    distances = pylab.array(distances) * 36
    total_heights = pylab.array([0] * len(distances))

    for h in heights:
        total_heights = total_heights + pylab.array(h)
    mean_heights = total_heights / len(heights)

    pylab.title('Trajectory of Projectile (Mean of 4 Trials)')
    pylab.xlabel('Inches from Launch Point')
    pylab.ylabel('Inches Above Launch Point')
    pylab.plot(distances, mean_heights, 'bo')

    a, b = pylab.polyfit(distances, mean_heights, 1)
    altitudes = a * distances + b
    pylab.plot(distances, altitudes, 'r', label='Linear Fit')

    a, b, c = pylab.polyfit(distances, mean_heights, 2)
    altitudes = a * (distances ** 2) + b * distances + c
    pylab.plot(distances, altitudes, 'g', label='Quadratic Fit')
    pylab.legend()


# try_fits('bow_and_arrow_1.txt')
# pylab.show()


def r_square(measured, estimated):
    """ss_res: one dimensional array of predicted values
       ss_tot: one dimensional array of measured values"""
    ss_res = ((measured - estimated) ** 2).sum()
    m_mean = measured.sum() / float(len(measured))
    ss_tot = ((measured - m_mean) ** 2).sum()
    return 1 - ss_res / ss_tot


def try_fits_1(file_name):
    distances, heights = get_trajectory_data(file_name)
    distances = pylab.array(distances) * 36
    total_heights = pylab.array([0] * len(distances))

    for h in heights:
        total_heights = total_heights + pylab.array(h)
    mean_heights = total_heights / len(heights)

    pylab.figure()
    pylab.title('Trajectory of Projectile (Mean of 4 Trials)')
    pylab.xlabel('Inches from Launch Point')
    pylab.ylabel('Inches Above Launch Point')
    pylab.plot(distances, mean_heights, 'bo')

    a, b = pylab.polyfit(distances, mean_heights, 1)
    altitudes = a * distances + b
    pylab.plot(
        distances,
        altitudes,
        'r',
        label='Linear Fit, R2 = {}'.format(round(r_square(mean_heights, altitudes), 4))
    )

    a, b, c = pylab.polyfit(distances, mean_heights, 2)
    altitudes = a * (distances ** 2) + b * distances + c
    pylab.plot(
        distances,
        altitudes,
        'g',
        label='Quadratic Fit, R2 = {}'.format(round(r_square(mean_heights, altitudes), 4))
    )
    pylab.legend()


# try_fits_1('bow_and_arrow_1.txt')
# pylab.show()


def get_x_speed(a, b, c, min_x, max_x):
    """min_x and max_x are distances in inches"""
    x_mid = (max_x - min_x) / 2
    y_peak = a * x_mid ** 2 + b * x_mid + c
    g = 32.16 * 12  # acceleration of gravity in inches/sec/sec
    t = (2 * y_peak / g) ** 0.5
    print('speed = {} feet/sec'.format(int(x_mid / (t * 12))))
    return x_mid / (t * 12)


def process_trajectories(file_name):
    distances, heights = get_trajectory_data(file_name)
    distances = pylab.array(distances) * 36
    total_heights = pylab.array([0] * len(distances))

    for h in heights:
        total_heights = total_heights + pylab.array(h)
    mean_heights = total_heights / len(heights)

    pylab.title('Trajectory of Projectile (Mean of 4 Trials)')
    pylab.xlabel('Inches from Launch Point')
    pylab.ylabel('Inches Above Launch Point')
    pylab.plot(distances, mean_heights, 'bo')

    a, b, c = pylab.polyfit(distances, mean_heights, 2)
    altitudes = a * (distances ** 2) + b * distances + c
    speed = get_x_speed(a, b, c, distances[-1], distances[0])
    pylab.plot(
        distances,
        altitudes,
        'g',
        label='Quadratic Fit, R2 = {0}, Speed = {1} feet/sec'.format(
            round(r_square(mean_heights, altitudes), 2),
            round(speed, 2)
        )
    )
    pylab.legend()


process_trajectories('bow_and_arrow_1.txt')
pylab.show()
