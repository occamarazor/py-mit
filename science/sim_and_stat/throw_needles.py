# Buffon's needle problem
# TODO: plot the data
import random


def std_dev(values):
    mean = sum(values) / len(values)
    total = 0
    for x in values:
        total += (x - mean) ** 2
    return (total / len(values)) ** 0.5


def throw_needles(needles):
    in_circle = 0
    for n in range(1, needles + 1):
        x = random.random()
        y = random.random()
        if (x * x + y * y) ** 0.5 <= 1:
            in_circle += 1
    return 4 * (in_circle / n)


def est_pi(needles=1000, precision=0.01, trials=20):
    s_dev = precision
    while s_dev >= (precision / 4):
        estimates = []
        for t in range(trials):
            pi_guess = throw_needles(needles)
            estimates.append(pi_guess)
        s_dev = std_dev(estimates)
        mean_est = sum(estimates) / len(estimates)

        print('Est. = {0}, Std. dev. = {1}, Needles = {2}'.format(mean_est, s_dev, needles))
        needles *= 2
    return mean_est


est_pi()
