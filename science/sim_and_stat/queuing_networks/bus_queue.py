# Shuttle bus simulation
import random
import pylab


class Job(object):
    def __init__(self, mean_inter_arrival, mean_job_time):
        # arrival rate of jobs
        self.inter_arrival = random.expovariate(1 / mean_inter_arrival)
        # time required to perform a job
        self.job_time = random.gauss(mean_job_time, mean_job_time / 2)
        # self.job_time = random.uniform(0, 2 * mean_job_time)
        self.queued_time = None
        # TODO: Handout code
        # self.started_time = None

    def get_inter_arrival(self):
        return self.inter_arrival

    def get_job_time(self):
        return self.job_time

    def get_queued_time(self):
        return self.queued_time

    def set_queued_time(self, time):
        self.queued_time = time
    # TODO: Handout code
    # def set_started_time(self, time):
    #     self.started_time = time
    #
    # def serve(self, server, time):
    #     if self.started_time is None:
    #         self.set_started_time(time)
    #     self.job_time -= server.serve()
    #
    #     if self.job_time <= 0:
    #         raise JobError(self.job_time, 'done')
    #
    # def __str__(self):
    #     return '<inter-arrival: {}, job time: {}, queued time: {}>'.format(
    #         self.inter_arrival,
    #         self.job_time,
    #         self.queued_time
    #     )


class Passenger(Job):
    # Arrival rate is for passenger to arrive at the bus stop
    # Job time is time for passenger to board the bus
    pass


class JobQueue(object):
    def __init__(self):
        self.jobs = []

    def add_job(self, job):
        self.jobs.append(job)

    def get_jobs_length(self):
        return len(self.jobs)
    # TODO: Handout code
    # def __str__(self):
    #     result = ''
    #     for j in self.jobs:
    #         result += '{}; '.format(j)
    #     return result


class FIFO(JobQueue):
    def remove_job(self):
        try:
            return self.jobs.pop(0)
        except IndexError:
            raise JobQueueEmptyError('remove_job() called with an empty queue')


class SRPT(JobQueue):
    def remove_job(self):
        try:
            least_index = 0
            for i in range(self.get_jobs_length()):
                if self.jobs[i].job_time < self.jobs[least_index].job_time:
                    least_index = i
            return self.jobs.pop(least_index)
        except IndexError:
            raise JobQueueEmptyError('remove_job() called with an empty queue')


class BusStop(SRPT):
    pass


class Bus(object):
    def __init__(self, speed, capacity):
        self.speed = speed
        self.capacity = capacity
        self.boarded = 0

    def get_speed(self):
        return self.speed

    def get_capacity(self):
        return self.capacity

    def get_boarded(self):
        return self.boarded

    def enter(self):
        if self.boarded < self.capacity:
            self.boarded += 1
        else:
            raise BusCapacityError(self.capacity, self.boarded, 'exceeded')

    def leave(self):
        if self.boarded > 0:
            self.boarded -= 1

    def unload(self, num):
        while num > 0:
            self.leave()
            num -= 1


class JobError(Exception):
    def __init__(self, job_time, message):
        self.job_time = job_time
        self.message = message

    def __str__(self):
        return 'Job {} in {}'.format(self.message, self.job_time)


class JobQueueEmptyError(Exception):
    def __init__(self, message):
        self.message = message


class BusCapacityError(Exception):
    def __init__(self, capacity, boarded, message):
        self.capacity = capacity
        self.boarded = boarded
        self.message = message

    def __str__(self):
        diff = self.boarded - self.capacity
        return 'The Bus capacity={} is {} by {}'.format(self.capacity, self.message, diff)


def sim_bus(bus, num_stops=6, loop_length=1200, mean_arrival=90, mean_job_time=10, sim_time=30000):
    time, total_wait, total_passengers, last_arrival, next_stop, bus_loc = [0] * 6
    stops = []
    average_wait_times = []
    next_job = Passenger(mean_arrival, mean_job_time)
    left_waiting = 0

    for n in range(num_stops):
        stops.append(BusStop())

    while time < sim_time:
        # advance & move the bus
        time += 1
        # assume speed is small relative to inter-stop distance
        bus_loc += bus.get_speed()
        # see if there is a passenger waiting to enter queue
        if last_arrival + next_job.get_inter_arrival() <= time:
            # passengers arrive simultaneously at each stop
            for stop in stops:
                stop.add_job(next_job)
            next_job.set_queued_time(time)
            last_arrival = time
            next_job = Job(mean_arrival, mean_job_time)

        # see if bus is at a stop
        if bus_loc % (loop_length / num_stops) == 0:
            # some passengers get off the bus
            bus.unload(bus.get_boarded() / num_stops)

            # all passengers who arrived prior to the bus's arrival
            # attempt to enter the bus
            while stops[next_stop].get_jobs_length() > 0:
                try:
                    bus.enter()
                except BusCapacityError:
                    break
                removed = stops[next_stop].remove_job()
                total_wait += time - removed.get_queued_time()
                total_passengers += 1
                # advance time but not the bus
                time += removed.get_job_time()

            if total_passengers == 0:
                average_wait_times.append(0)
            else:
                average_wait_times.append(total_wait / total_passengers)

            # passengers might have arrived at stops while bus is loading
            while last_arrival + next_job.get_inter_arrival() <= time:
                for stop in stops:
                    stop.add_job(next_job)
                next_job.set_queued_time(time)
                last_arrival += next_job.get_inter_arrival()
                next_job = Job(mean_arrival, mean_job_time)

            if next_stop < len(stops) - 1:
                next_stop += 1
            else:
                next_stop = 0

    for stop in stops:
        left_waiting += stop.get_jobs_length()
    return average_wait_times, left_waiting


def test(capacities, speeds, trials_num):
    random.seed(0)

    for cap in capacities:
        for speed in speeds:
            # keep track of 1st 500
            total_wait_times = pylab.array([0.0] * 500)
            total_left_waiting = 0

            for t in range(trials_num):
                average_wait_times, left_waiting = sim_bus(Bus(cap, speed))
                total_wait_times = pylab.array(average_wait_times[:500])
                total_left_waiting += left_waiting

            average_wait_times = total_wait_times / trials_num
            left_waiting = int(total_left_waiting / trials_num)
            lab = 'Speed = {}, Cap = {}, Left = {}'.format(speed, cap, left_waiting)
            pylab.plot(average_wait_times, label=lab)

    pylab.xlabel('Stop number')
    pylab.ylabel('Aggregate average wait time')
    pylab.title('Impact of bus speed and capacity')

    y_min, y_max = pylab.ylim()
    if y_max - y_min > 200:
        pylab.semilogy()
    pylab.ylim(bottom=1)
    pylab.legend(loc='lower right')


test([30], [10], 40)
test([15], [10], 40)
test([15], [20], 40)
pylab.show()

# TODO: Handout code
# def sim_bus_handout(capacity=25, num_stops=6, speed=600, mean_arrival=90, mean_job_time=10, num_loops=75):
#     bus = Bus(speed, capacity)
#     between_stops = round(speed / num_stops)
#     loops, time, total_wait, total_passengers, last_arrival = [0] * 5
#     stops = []
#     average_wait_times = []
#     round_trip_times = []
#     last_trip_start = 0
#     next_stop = 0
#
#     for n in range(num_stops):
#         stops.append(BusStop())
#     next_job = Passenger(mean_arrival, mean_job_time)
#
#     for s in range(num_loops * num_stops):
#         for t in range(between_stops):
#             if last_arrival + next_job.get_inter_arrival() <= time:
#                 for stop in stops:
#                     stop.add_job(next_job)
#                 next_job.set_queued_time(time)
#                 last_arrival = time
#                 next_job = Passenger(mean_arrival, mean_job_time)
#             time += 1
#         to_leave = bus.get_boarded() / num_stops
#
#         while to_leave > 0:
#             bus.leave()
#             to_leave -= 1
#
#         while stops[next_stop].get_jobs_number() > 0:
#             try:
#                 bus.enter()
#             except BusCapacityError:
#                 break
#             j = stops[next_stop].remove_job()
#             total_wait += time - j.get_queued_time()
#             total_passengers += 1
#             time += j.get_job_time()
#
#             while last_arrival + next_job.get_inter_arrival() <= time:
#                 for stop in stops: stop.add_job(next_job)
#                 next_job.set_queued_time(time)
#                 last_arrival = time
#                 next_job = Passenger(mean_arrival, mean_job_time)
#         if next_stop < len(stops) - 1:
#             next_stop += 1
#         else:
#             round_trip_times.append(time - last_trip_start)
#             last_trip_start = time
#             next_stop = 0
#         aveWait = total_wait / total_passengers
#         average_wait_times.append(aveWait)
#
#     x_axis = pylab.arange(0, len(average_wait_times), 1)
#     pylab.plot(x_axis, average_wait_times)
#     pylab.xlabel('Stop Number')
#     pylab.ylabel('Aggregate Average Wait Time')
#
#     pylab.figure()
#     x_axis = pylab.arange(0, len(round_trip_times), 1)
#     pylab.plot(x_axis, round_trip_times)
#     pylab.xlabel('Round Trip')
#     pylab.ylabel('Round Trip Time')
#     pylab.show()
#
#
# def test_cap_handout():
#     while True:
#         capacity = input('Enter bus capacity, or a non integer to end: ')
#
#         try:
#             cap = int(capacity)
#             sim_bus_handout(cap)
#         except:
#             break
#
#
# def test_combo_handout():
#     while True:
#         try:
#             capacity = input('Enter bus capacity, or a non integer to end: ')
#             cap = int(capacity)
#             speed = input('Enter round trip time, or a non integer to end: ')
#             speed = int(speed)
#             sim_bus_handout(capacity=cap, speed=speed)
#         except:
#             print('Exiting')
#             break
