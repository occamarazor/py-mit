# Poisson process example
import random
import pylab


mean_arrival = 60
arrivals = []

for i in range(2000):
    inter_arrival_time = random.expovariate(1 / mean_arrival)
    arrivals.append(inter_arrival_time)
average = sum(arrivals) / len(arrivals)
print('Distance from intended mean:', mean_arrival - average)

x_axis = pylab.arange(0, len(arrivals), 1)
pylab.scatter(x_axis, arrivals)
pylab.axhline(mean_arrival, linewidth=4, color='black')
pylab.title('Exponential Inter-arrival Times')
pylab.ylabel('Inter-arrival Time (secs)')
pylab.xlabel('Job Number')

pylab.figure()
pylab.hist(arrivals, edgecolor='black')
pylab.title('Exponential Inter-arrival Times')
pylab.xlabel('Inter-arrival Time (secs)')
pylab.ylabel('Number of Jobs')
pylab.show()
