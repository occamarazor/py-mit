# Avoiding statistical fallacies
import random
import pylab


# Details of data matter: same linear fit but data significantly differs
def anscombe(plot_points):
    data_file = open('anscombe_data.txt')
    coord_groups = [[] for _ in range(8)]
    x_groups = []
    y_groups = []
    x_vals = pylab.array(range(21))

    for line in data_file:
        if line[0] == '#':
            continue
        line_group = line.split()
        for i in range(len(line_group)):
            coord_groups[i].append(float(line_group[i]))

    for i in range(len(coord_groups)):
        if i % 2 == 0:
            x_groups.append(coord_groups[i])
        else:
            y_groups.append(coord_groups[i])

    data_file.close()

    for i in range(len(x_groups)):
        x_list = x_groups[i]
        y_list = y_groups[i]

        pylab.figure()
        if plot_points:
            pylab.plot(x_list, y_list, 'o')
        a, b = pylab.polyfit(x_list, y_list, 1)
        y_vals = a * x_vals + b
        pylab.plot(x_vals, y_vals)
        pylab.xlim(0, 20)
        mean = sum(y_list) / float(len(y_list))
        median = y_list[len(y_list) // 2 + 1]
        pylab.title('Mean = ' + str(mean) + ', Median = ' + str(median))

    pylab.show()


# anscombe(True)


# Data extrapolation: 115% of population in 2010 using internet???
def internet(points):
    years = pylab.array(range(1994, 2001))
    percent = [4.9, 9.4, 16.7, 22, 30.7, 36.6, 43.9]
    pylab.plot(years, percent)
    a, b = pylab.polyfit(years, percent, 1)
    y_vals = a * pylab.array(years) + b
    pylab.plot(years, y_vals)
    pylab.title('Internet Usage in the United States')
    pylab.xlabel('Year')
    pylab.ylabel('% of Population')

    if points:
        pylab.figure()
        pylab.plot(years, percent, 'o')
        x_vals = pylab.array(range(1994, 2011))
        y_vals = a * x_vals + b
        pylab.plot(x_vals, y_vals)
        pylab.title('Projected Internet Usage in the United States')
        pylab.xlabel('Year')
        pylab.ylabel('% of Population')
    pylab.show()


# internet(True)


# Texas sharpshooter fallacy
# Sample: 446 women diagnosed as anorexic
# 446 / 12 = 37 women born each month
# 48 women born in June???
def june_prob(num_trials):
    total_women = 446
    months = range(1, 13)
    june_48 = 0

    for trial in range(num_trials):
        june = 0
        for i in range(total_women):
            if random.choice(months) == 6:
                june += 1
        if june >= 48:
            june_48 += 1

    j_prob = str(june_48 / num_trials)
    print('Probability of at least 48 births in June = ' + j_prob)


# june_prob(1000)


def any_prob(num_trials):
    total_women = 446
    any_month = 0

    for trial in range(num_trials):
        months = [0] * 13
        for i in range(total_women):
            months[random.choice(range(1, 13))] += 1
        if max(months) >= 48:
            any_month += 1

    a_prob = str(any_month / num_trials)
    print('Probability of at least 48 births in some month = ' + a_prob)


any_prob(1000)
