# Monty Hall problem
import random
import pylab


def monty_choose(guess_door, prize_door):
    if 1 != guess_door and 1 != prize_door:
        return 1
    if 2 != guess_door and 2 != prize_door:
        return 2
    return 3


def random_choose(guess_door, prize_door):
    if guess_door == 1:
        return random.choice([2, 3])
    if guess_door == 2:
        return random.choice([1, 3])
    return random.choice([1, 2])


def sim_monty_hall(trials=100, door_choose=monty_choose):
    stick_wins = 0
    switch_wins = 0
    no_win = 0
    door_choices = [1, 2, 3]

    for t in range(trials):
        guess_door = random.choice(door_choices)
        prize_door = random.choice(door_choices)
        door_to_open = door_choose(guess_door, prize_door)

        if door_to_open == prize_door:
            no_win += 1
        elif guess_door == prize_door:
            stick_wins += 1
        else:
            switch_wins += 1
    return stick_wins, switch_wins


def display_sim(sim_res):
    stick_wins, switch_wins = sim_res
    pylab.pie([stick_wins, switch_wins], colors=['r', 'g'],
              labels=['stick', 'switch'], autopct='%.2f%%')
    pylab.title('To switch or not to switch')


sim_results = sim_monty_hall(100000)
display_sim(sim_results)
pylab.figure()
sim_results = sim_monty_hall(100000, random_choose)
display_sim(sim_results)
pylab.show()
