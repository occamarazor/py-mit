import random
from cluster import Cluster


def k_means(points, k, cutoff, max_iters, point_type, print_steps):
    assert len(points) >= k
    # Get k randomly chosen initial centroids
    initial_centroids = random.sample(points, k)
    clusters = []
    # Create a singleton cluster for each centroid
    for i in initial_centroids:
        clusters.append(Cluster([i], point_type))
    num_iters = 0
    biggest_change = cutoff

    while biggest_change >= cutoff and num_iters < max_iters:
        # Create a list containing k empty lists
        new_clusters = []
        for i in range(k):
            new_clusters.append([])
        for p in points:
            # Find the centroid closest to p
            smallest_distance = p.distance(clusters[0].get_centroid())
            index = 0

            for i in range(k):
                distance = p.distance(clusters[i].get_centroid())
                if distance < smallest_distance:
                    smallest_distance = distance
                    index = i
            # Add p to the list of points for the appropriate cluster
            new_clusters[index].append(p)
        # Update each cluster and record how much the centroid has changed
        biggest_change = 0
        for i in range(len(clusters)):
            change = clusters[i].update(new_clusters[i])
            biggest_change = max(biggest_change, change)
        num_iters += 1
    # Calculate the coherence of the least coherent cluster
    max_dist = 0
    for c in clusters:
        for p in c.members():
            if p.distance(c.get_centroid()) > max_dist:
                max_dist = p.distance(c.get_centroid())
    print('Number of iterations = {0}, Max Diameter = {1}'.format(num_iters, max_dist))
    return clusters, max_dist
