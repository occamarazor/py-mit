# An abstract object to be clustered
class Point(object):
    def __init__(self, name, original_attrs, normalized_attrs):
        """original_attrs and normalized_attrs are both arrays"""
        self.name = name
        self.original_attrs = original_attrs
        if normalized_attrs is None:
            self.attrs = original_attrs
        else:
            self.attrs = normalized_attrs

    def dimensionality(self):
        return len(self.attrs)

    def set_attrs(self, new_attrs):
        self.attrs = new_attrs

    def get_attrs(self):
        return self.attrs

    def get_original_attrs(self):
        return self.original_attrs

    def distance(self, other):
        # Euclidean distance metric
        result = 0
        for i in range(self.dimensionality()):
            result += (self.attrs[i] - other.attrs[i]) ** 2
        return result ** 0.5

    def get_name(self):
        return self.name

    def to_str(self):
        return self.name + str(self.attrs)

    def __str__(self):
        return self.name
