import pylab


# A set of Points
class Cluster(object):
    def __init__(self, points, point_type):
        self.points = points
        self.point_type = point_type
        self.centroid = self.compute_centroid()

    def single_linkage_dist(self, other):
        min_dist = self.points[0].distance(other.points[0])
        for p1 in self.points:
            for p2 in other.points:
                if p1.distance(p2) < min_dist:
                    min_dist = p1.distance(p2)
        return min_dist

    def max_linkage_dist(self, other):
        max_dist = self.points[0].distance(other.points[0])
        for p1 in self.points:
            for p2 in other.points:
                if p1.distance(p2) > max_dist:
                    max_dist = p1.distance(p2)
        return max_dist

    def average_linkage_dist(self, other):
        total_dist = 0
        for p1 in self.points:
            for p2 in other.points:
                total_dist += p1.distance(p2)
        return total_dist / (len(self.points) * len(other.points))

    def update(self, points):
        old_centroid = self.centroid
        self.points = points
        if len(points) > 0:
            self.centroid = self.compute_centroid()
            return old_centroid.distance(self.centroid)
        else:
            return 0

    def members(self):
        for p in self.points:
            yield p

    def is_in(self, name):
        for p in self.points:
            if p.get_name() == name:
                return True
        return False

    def to_str(self):
        result = ''
        for p in self.points:
            result += p.to_str() + ', '
        return result[:-2]

    def __str__(self):
        names = []
        for p in self.points:
            names.append(p.get_name())
        names.sort()
        result = ''
        for p in names:
            result += p + ', '
        return result[:-2]

    def get_centroid(self):
        return self.centroid

    def compute_centroid(self):
        dim = self.points[0].dimensionality()
        total_values = pylab.array([0] * dim)
        for p in self.points:
            total_values = total_values + p.get_attrs()

        centroid = self.point_type(
            'mean',
            total_values / len(self.points),
            total_values / len(self.points)
        )
        return centroid
