from cluster import Cluster


# A set of Clusters
class ClusterSet(object):
    def __init__(self, point_type):
        self.members = []

    def add(self, c):
        if c in self.members:
            raise ValueError
        self.members.append(c)

    def get_clusters(self):
        return self.members[:]

    def find_closest(self, metric):
        min_distance = metric(self.members[0], self.members[1])
        to_merge = (self.members[0], self.members[1])
        for c1 in self.members:
            for c2 in self.members:
                if c1 == c2:
                    continue
                if metric(c1, c2) < min_distance:
                    min_distance = metric(c1, c2)
                    to_merge = (c1, c2)
        return to_merge

    def merge_clusters(self, c1, c2):
        points = []
        for p in c1.members():
            points.append(p)
        for p in c2.members():
            points.append(p)
        new_c = Cluster(points, type(p))
        self.members.remove(c1)
        self.members.remove(c2)
        self.add(new_c)
        return c1, c2

    def merge_one(self, metric, print_steps):
        if len(self.members) == 1:
            return None
        if len(self.members) == 2:
            return self.merge_clusters(self.members[0], self.members[1])

        to_merge = self.find_closest(metric)
        if print_steps:
            print('Merged {0} with {1}'.format(to_merge[0], to_merge[1]))
        self.merge_clusters(to_merge[0], to_merge[1])
        return to_merge

    def merge_n(self, metric, num_clusters, print_steps, history=[]):
        assert num_clusters >= 1
        while len(self.members) > num_clusters:
            merged = self.merge_one(metric, print_steps)
            history.append(merged)
        return history

    def num_clusters(self):
        return len(self.members) + 1

    def __str__(self):
        result = ''
        for c in self.members:
            result += str(c) + '\n'
        return result
