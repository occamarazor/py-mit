# Agglomerative hierarchical clustering of mammals' dental records (teeth sets)
# Purpose: Infer mammals' eating habits
import pylab
import copy
from point import Point
from cluster import Cluster
from cluster_set import ClusterSet
from k_means import k_means


# Mammal's teeth example
class Mammal(Point):
    def __init__(self, name, original_attrs, normalized_attrs=None):
        Point.__init__(self, name, original_attrs, normalized_attrs)

    def normalize_features(self, scaling_key):
        scale_dict = {
            'identity': [1, 1, 1, 1, 1, 1, 1, 1],
            '1/max':    [1 / 3, 1 / 4, 1, 1, 1 / 4, 1 / 4, 1 / 6, 1 / 6]  # 1/max number of each tooth
        }
        original_attrs = self.get_original_attrs()
        normalized_attrs = []

        for i in range(len(original_attrs)):
            normalized_attrs.append(original_attrs[i] * scale_dict[scaling_key][i])
        self.set_attrs(normalized_attrs)


def read_mammal_data(file_name):
    data_file = open(file_name)
    teeth_list = []
    name_list = []

    for line in data_file:
        if len(line) == 0 or line[0] == '#':
            continue
        data_line = str.split(line)
        teeth = data_line.pop(-1)
        features = []
        name = ''

        for t in teeth:
            features.append(int(t))
        for w in data_line:
            name += w + ' '

        name = name[:-1]
        teeth_list.append(features)
        name_list.append(name)
    return name_list, teeth_list


def build_mammal_points(file_name, scaling_key):
    name_list, feature_list = read_mammal_data(file_name)
    points = []
    for i in range(len(name_list)):
        point = Mammal(name_list[i], pylab.array(feature_list[i]))
        point.normalize_features(scaling_key)
        points.append(point)
    return points


# Use hierarchical clustering for mammals teeth
def test_hierarchical(file_name='mammals_data.txt', num_clusters=2, scaling_key='identity', print_steps=False,
                      print_history=False):
    points = build_mammal_points(file_name, scaling_key)
    c_s = ClusterSet(Mammal)
    for p in points:
        c_s.add(Cluster([p], Mammal))

    history = c_s.merge_n(Cluster.max_linkage_dist, num_clusters, print_steps)
    if print_history:
        for i in range(len(history)):
            names1 = []
            names2 = []

            for p in history[i][0].members():
                names1.append(p.get_name())
            for p in history[i][1].members():
                names2.append(p.get_name())
            print('Step {0} merged {1} with {2}\n'.format(i, names1, names2))

    clusters = c_s.get_clusters()
    print('Final set of clusters:')
    index = 0
    for c in clusters:
        print('  C{0}: {1}'.format(index, c))
        index += 1


# test_hierarchical(print_history=True)  # no normalizing (different teeth types have different dispersion)
# test_hierarchical(scaling_key='1/max')  # scaling by max teeth


def test_k_means(file_name='mammals_data.txt', num_clusters=2, scaling_key='1/max', cutoff=0.0001, max_iters=100,
                 num_trials=1, print_steps=False, print_history=False):
    points = build_mammal_points(file_name, scaling_key)
    if print_steps:
        print('Points:')
        for p in points:
            attrs = p.get_original_attrs()
            for i in range(len(attrs)):
                attrs[i] = round(attrs[i], 2)
            print('  {0}, {1}'.format(p, attrs))
    num_clusterings = 0
    best_diameter = None
    while num_clusterings < num_trials:
        clusters, max_diameter = k_means(points, num_clusters, cutoff, max_iters, Mammal, print_steps)
        if best_diameter is None or max_diameter < best_diameter:
            best_diameter = max_diameter
            best_clustering = copy.deepcopy(clusters)
        if print_history:
            print('Clusters:')
            for i in range(len(clusters)):
                print('  C {0}: {1}'.format(i, clusters[i]))
        num_clusterings += 1
    print('Best Clustering')
    for i in range(len(best_clustering)):
        print('  C{0}: {1}'.format(i, best_clustering[i]))


test_k_means()
