# Agglomerative hierarchical clustering of US counties (distance from each other)
import pylab
import copy
from point import Point
from cluster import Cluster
from cluster_set import ClusterSet
from k_means import k_means


# US Counties example
class County(Point):
    # Interesting subsets of features
    # 0=don't use, 1=use
    all_features = (
        ('HomeVal', '1'),
        ('Income', '0'),
        ('Poverty', '1'),
        ('Population', '1'),
        ('Pop Change', '1'),
        ('Percent 65+', '1'),
        ('Below 18', '1'),
        ('Percent Female', '1'),
        ('Percent HS Grad', '1'),
        ('Percent College', '1'),
        ('Unemployed', '1'),
        ('Percent Below 18', '1'),
        ('Life Expect', '1'),
        ('Farm Acres', '1')
    )
    education = (
        ('HomeVal', '0'),
        ('Income', '0'),
        ('Poverty', '0'),
        ('Population', '0'),
        ('Pop Change', '0'),
        ('Percent 65+', '0'),
        ('Below 18', '0'),
        ('Percent Female', '0'),
        ('Percent HS Grad', '1'),
        ('Percent College', '1'),
        ('Unemployed', '0'),
        ('Percent Below 18', '0'),
        ('Life Expect', '0'),
        ('Farm Acres', '0')
    )
    no_education = (
        ('HomeVal', '1'),
        ('Income', '0'),
        ('Poverty', '1'),
        ('Population', '1'),
        ('Pop Change', '1'),
        ('Percent 65+', '1'),
        ('Below 18', '1'),
        ('Percent Female', '1'),
        ('Percent HS Grad', '1'),
        ('Percent College', '0'),
        ('Unemployed', '0'),
        ('Percent Below 18', '1'),
        ('Life Expect', '1'),
        ('Farm Acres', '1')
    )
    wealth_only = (
        ('HomeVal', '1'),
        ('Income', '1'),
        ('Poverty', '1'),
        ('Population', '0'),
        ('Pop Change', '0'),
        ('Percent 65+', '0'),
        ('Below 18', '0'),
        ('Percent Female', '0'),
        ('Percent HS Grad', '0'),
        ('Percent College', '0'),
        ('Unemployed', '1'),
        ('Percent Below 18', '0'),
        ('Life Expect', '0'),
        ('Farm Acres', '0')
    )
    no_wealth = (
        ('HomeVal', '0'),
        ('Income', '0'),
        ('Poverty', '0'),
        ('Population', '1'),
        ('Pop Change', '1'),
        ('Percent 65+', '1'),
        ('Below 18', '1'),
        ('Percent Female', '1'),
        ('Percent HS Grad', '1'),
        ('Percent College', '1'),
        ('Unemployed', '0'),
        ('Percent Below 18', '1'),
        ('Life Expect', '1'),
        ('Farm Acres', '1')
    )
    gender = (
        ('HomeVal', '0'),
        ('Income', '0'),
        ('Poverty', '0'),
        ('Population', '0'),
        ('Pop Change', '0'),
        ('Percent 65+', '0'),
        ('Below 18', '0'),
        ('Percent Female', '1'),
        ('Percent HS Grad', '0'),
        ('Percent College', '0'),
        ('Unemployed', '0'),
        ('Percent Below 18', '0'),
        ('Life Expect', '0'),
        ('Farm Acres', '0')
    )
    filter_names = {
        'all': all_features,
        'education': education,
        'no_education': no_education,
        'wealth_only': wealth_only,
        'no_wealth': no_wealth,
        'gender': gender
    }
    attr_filter = ''

    # Override Point to construct subset of features
    def __init__(self, name, original_attrs, normalized_attrs, filter_name='all'):
        Point.__init__(self, name, original_attrs, normalized_attrs)

        if County.attr_filter == '':
            filter_spec = County.filter_names[filter_name]
            for feature in filter_spec:
                County.attr_filter += feature[1]

        features = []
        for i in range(len(County.attr_filter)):
            if County.attr_filter[i] == '1':
                features.append((self.get_attrs()[i]))
        self.features = features

    def distance(self, other):
        result = 0
        for i in range(len(self.features)):
            result += (self.features[i] - other.features[i]) ** 2
        return result ** 0.5


def read_county_data(file_name, num_entries=14):
    data_file = open(file_name)
    data_list = []
    name_list = []
    max_vals = pylab.array([0] * num_entries)

    # Build unnormalized feature vector
    for line in data_file:
        if len(line) == 0 or line[0] == '#':
            continue
        data_line = str.split(line)
        name = data_line[0] + data_line[1]
        features = []

        # Build vector with num_entries features
        for char_set in data_line[2:]:
            try:
                char_set = float(char_set)
                features.append(char_set)
                if char_set > max_vals[len(features) - 1]:
                    max_vals[len(features) - 1] = char_set
            except ValueError:
                name = name + char_set

        if len(features) != num_entries:
            continue
        data_list.append(features)
        name_list.append(name)
    return name_list, data_list, max_vals


def build_county_points(file_name, filter_name, scaling):
    name_list, feature_list, max_vals = read_county_data(file_name)
    points = []
    for i in range(len(name_list)):
        original_attrs = pylab.array(feature_list[i])
        if scaling:
            normalized_attrs = original_attrs / pylab.array(max_vals)
        else:
            normalized_attrs = original_attrs
        points.append(County(name_list[i], original_attrs, normalized_attrs, filter_name))
    return points


def get_average_income(cluster):
    tot = 0
    num_elems = 0
    for c in cluster.members():
        tot += c.get_original_attrs()[1]
        num_elems += 1
    if num_elems > 0:
        return tot / num_elems
    return 0


# Use hierarchical clustering for counties
def test_hierarchical(file_name='counties_new_england.txt', filter_name='all', num_clusters=2, scaling=True,
                      print_steps=False, print_history=False):
    points = build_county_points(file_name, filter_name, scaling)
    c_s = ClusterSet(County)
    for p in points:
        c_s.add(Cluster([p], County))

    history = c_s.merge_n(Cluster.max_linkage_dist, num_clusters, print_steps)
    if print_history:
        for i in range(len(history)):
            names1 = []
            names2 = []

            for p in history[i][0].members():
                names1.append(p.get_name())
            for p in history[i][1].members():
                names2.append(p.get_name())
            print('Step {0} merged {1} with {2}\n'.format(i, names1, names2))

    clusters = c_s.get_clusters()
    print('Final set of clusters:')
    index = 0
    for c in clusters:
        print('  C{0}: {1}'.format(index, c))
        index += 1


# no normalizing (Middlesex has a too large population)
# test_hierarchical(scaling=False, print_history=True)
# test_hierarchical()  # autoscaling


def test_k_means(file_name='counties_data.txt', filter_name='all', num_clusters=50,
                 scaling=True, cutoff=0.01, max_iters=100, num_trials=1, my_home='MAMiddlesex',
                 print_steps=False, print_history=False):
    # Build the set of points
    points = build_county_points(file_name, filter_name, scaling)
    if print_steps:
        print('Points')

        for p in points:
            attrs = p.get_attrs()
            for i in range(len(attrs)):
                attrs[i] = round(attrs[i], 2)
            print('  {0}, {1}'.format(p, attrs))
    num_clusterings = 0
    best_distance = None

    # Run k-means multiple times and choose best
    while num_clusterings < num_trials:
        print('Starting clustering number', num_clusterings)
        clusters, max_smallest = k_means(points, num_clusters, cutoff, max_iters, County, print_steps)
        num_clusterings += 1
        if best_distance is None or max_smallest < best_distance:
            best_distance = max_smallest
            best_clustering = copy.deepcopy(clusters)
        if print_history:
            print('Clusters:')
        for i in range(len(clusters)):
            if print_history:
                print('  C{0}: {1}'.format(i, clusters[i]))

    for c in best_clustering:
        incomes = []
        for i in range(len(best_clustering)):
            incomes.append(get_average_income(best_clustering[i]))
            if print_history:
                print('  C{0}: {1}'.format(i, best_clustering[i]))
        pylab.hist(incomes, edgecolor='k')
        pylab.xlabel('Average Income')
        pylab.ylabel('Number of Clusters')
        if c.is_in(my_home):
            print('Home Cluster:', c)
            print('Average income =', round(get_average_income(c), 0))


# test_k_means(file_name='counties_new_england.txt', num_clusters=2, print_history=True)
#
# test_k_means(file_name='counties_new_england.txt', num_clusters=20, num_trials=2, filter_name='education')
# pylab.title('Education Level (New England)')
#
# pylab.figure()
# test_k_means()
# pylab.title('All Features')
#
# pylab.figure()
# test_k_means(filter_name='education')
# pylab.title('Education Level')
#
# pylab.figure()
# test_k_means(num_trials=5, filter_name='education')
# pylab.title('Education Level (5 trials)')
#
# pylab.figure()
# test_k_means(filter_name='wealth_only')
# pylab.title('Home Value, Income, Unemployed')

pylab.figure()
test_k_means(filter_name='gender')
pylab.title('Gender')

pylab.show()
