# O(n^2)
def selection_sort(arr):
    print('initial arr: ', arr)
    for i in range(len(arr) - 1):
        min_indx = i  # invariant
        min_val = arr[i]
        j = i + 1
        while j < len(arr):
            if min_val > arr[j]:
                min_indx = j
                min_val = arr[j]
            j += 1
        temp = arr[i]
        arr[i] = arr[min_indx]
        arr[min_indx] = temp
        print('partial sort:', arr)
    return {'sorted arr': arr}


print(selection_sort([35, 4, 5, 58, 29, 0, 17]))
