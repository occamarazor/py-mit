# Using bruteforce algorithm to solve the 0/1 knapsack problem
from build_items import *


def decimal_to_bin(num, digits_num):
    """requires: n is a natural number less than 2**num_digits
       returns: a binary string of length num_digits representing the decimal number n."""
    assert type(num) == int and type(digits_num) == int and num >= 0 and num < 2 ** digits_num
    b_str = ''

    while num > 0:
        b_str = str(num % 2) + b_str
        num = num // 2
    while digits_num - len(b_str) > 0:
        b_str = '0' + b_str
    return b_str


def generate_power_set(items):
    """Generate a list of lists representing the power set of items"""
    num_subsets = 2 ** len(items)
    templates = []

    for i in range(num_subsets):
        templates.append(decimal_to_bin(i, len(items)))
    p_set = []

    for t in templates:
        elem = []
        for j in range(len(t)):
            if t[j] == '1':
                elem.append(items[j])
        p_set.append(elem)

    return p_set


def choose_best(p_set, constraint, get_value, get_weight):
    best_value = 0
    best_set = None

    for items in p_set:
        items_value = 0
        items_weight = 0

        for item in items:
            items_value += get_value(item)
            items_weight += get_weight(item)

        if items_weight <= constraint and items_value > best_value:
            best_value = items_value
            best_set = items

    return best_set, best_value


def test_best():
    items = build_items()
    p_set = generate_power_set(items)
    taken, value = choose_best(p_set, 20, Item.get_value, Item.get_weight)
    print('Total value of items taken = {}'.format(value))

    for item in taken:
        print('  {}'.format(item))


test_best()
