import random


class Item(object):
    def __init__(self, n, v, w):
        self.name = n
        self.value = v
        self.weight = w

    def get_name(self):
        return self.name

    def get_value(self):
        return self.value

    def get_weight(self):
        return self.weight

    def __str__(self):
        result = '<{}, {}, {}>'.format(self.name, self.value, self.weight)
        return result


def build_items():
    names = ['clock', 'painting', 'radio', 'vase', 'book', 'computer']
    values = [175, 90, 20, 50, 10, 200]
    weights = [10, 9, 4, 2, 1, 20]
    items = []

    for i in range(len(values)):
        items.append(Item(names[i], values[i], weights[i]))
    return items


def build_many_items(num_items, max_val, max_weight):
    items = []
    for i in range(num_items):
        items.append(
            Item(str(i),
                 random.randrange(1, max_val),
                 random.randrange(1, max_weight)
                 )
            # Item(str(i),
            #      random.randrange(1, max_val),
            #      random.randrange(1, max_weight) * random.random()
            #      )
        )
    return items
