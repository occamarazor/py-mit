# Using binary decision tree VS pseudo-polynomial algo to solve the 0/1 knapsack problem
from build_items import *
import random
import time
import sys


# No memoization
# Binary tree has 2**n nodes
def solve(item_set, max_weight):
    global num_calls
    num_calls += 1

    if item_set == [] or max_weight == 0:
        result = (0, ())
    elif item_set[0].get_weight() > max_weight:
        result = solve(item_set[1:], max_weight)
    else:
        item = item_set[0]
        # Explore left branch
        with_val, with_to_take = solve(item_set[1:], max_weight - item.get_weight())
        with_val += item.get_value()
        # Explore right branch
        without_val, without_to_take = solve(item_set[1:], max_weight)
        # Choose better branch
        if with_val > without_val:
            result = (with_val, with_to_take + (item,))
        else:
            result = (without_val, without_to_take)
    return result


def small_test(num_items=16, max_item_val=200, max_item_weight=20):
    global num_calls
    num_calls = 0
    max_weight = 8 * max_item_weight

    # items = build_items()
    items = build_many_items(num_items, max_item_val, max_item_weight)
    val, taken = solve(items, max_weight)

    print('Total value of items taken = ' + str(val))
    print('Number of calls =', str(num_calls))


# small_test()

# The same tree + memoization
# Dict lookup is O(1)
def fast_solve(item_set, max_weight, memo=None):
    global num_calls
    num_calls += 1

    # Initialize for first invocation
    if memo is None:
        memo = {}
    if (len(item_set), max_weight) in memo:
        # Use solution found earlier
        result = memo[(len(item_set), max_weight)]
        return result
    elif item_set == [] or max_weight == 0:
        result = (0, ())
    elif item_set[0].get_weight() > max_weight:
        # Loop off first item in item_set and solve
        result = fast_solve(item_set[1:], max_weight, memo)
    else:
        item = item_set[0]
        # Consider taking first item
        with_val, with_to_take = fast_solve(item_set[1:], max_weight - item.get_weight(), memo)
        with_val += item.get_value()

        # Consider not taking first item
        without_val, without_to_take = fast_solve(item_set[1:], max_weight, memo)

        # Choose better alternative
        if with_val > without_val:
            result = (with_val, with_to_take + (item,))
        else:
            result = (without_val, without_to_take)
    # Update memo
    memo[(len(item_set), max_weight)] = result
    return result


# Max recursion depth increased
sys.setrecursionlimit(2000)


def test(max_item_val=200, max_item_weight=20, run_slowly=False):
    random.seed(0)
    global num_calls
    max_weight = 8 * max_item_weight
    print('#items, #num taken, Value, Solver, #calls, time')

    for num_items in (4, 8, 16, 32, 64, 128, 256, 512, 1024):
        # items = build_items()
        items = build_many_items(num_items, max_item_val, max_item_weight)

        if run_slowly:
            tests = (fast_solve, solve)
        else:
            tests = (fast_solve,)

        for func in tests:
            num_calls = 0
            start_time = time.time()
            val, to_take = func(items, max_weight)
            elapsed = time.time() - start_time
            func_name = func.__name__
            print(num_items, len(to_take), val, func_name, num_calls, elapsed)


# test(run_slowly=True)
test()
