# Using greedy algorithm to solve the 0/1 knapsack problem
from build_items import build_items


def greedy(items, max_weight, key_fcn):
    assert type(items) == list and max_weight >= 0

    items_copy = sorted(items, key=key_fcn, reverse=True)  # 1) mergesort: O(n * log(n))
    result = []
    total_value = 0
    total_weight = 0
    i = 0

    while total_weight < max_weight and i < len(items):  # 2) O(n)
        if (total_weight + items_copy[i].get_weight()) <= max_weight:
            result.append((items_copy[i]))
            total_weight += items_copy[i].get_weight()
            total_value += items_copy[i].get_value()
        i += 1
    return result, total_value


def value(item):
    return item.get_value()


def weight_inverse(item):
    return 1 / item.get_weight()


def density(item):
    return item.get_value() / item.get_weight()


def test_greedy(items, constraint, get_key):
    taken, val = greedy(items, constraint, get_key)
    print('Total value of items taken = {}'.format(val))
    for item in taken:
        print('  {}'.format(item))


def test_greedies(max_weight=20):
    items = build_items()
    print('items to choose from:')
    for item in items:
        print('  {}'.format(item))

    print('Use greedy by value to fill a knapsack of size', max_weight)
    test_greedy(items, max_weight, value)
    print('Use greedy by weight to fill a knapsack of size', max_weight)
    test_greedy(items, max_weight, weight_inverse)
    print('Use greedy by density to fill a knapsack of size', max_weight)
    test_greedy(items, max_weight, density)


test_greedies()
