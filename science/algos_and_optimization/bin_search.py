# O(log n)
def bin_search(arr, num):
    steps = 0
    min_index = 0
    max_index = len(arr) - 1

    while min_index <= max_index:
        current_index = int((min_index + max_index) / 2)
        current = arr[current_index]

        if current > num:
            steps += 1
            max_index = current_index - 1
        elif current < num:
            steps += 1
            min_index = current_index + 1
        else:
            steps += 1
            return {'search steps': steps, 'index': current_index}
    return {'search steps': steps, 'index': 'no such number found'}


print(bin_search(list(range(0, 101)), 0))
print(bin_search(list(range(0, 101)), 50))
print(bin_search(list(range(0, 101)), 100))
print(bin_search(list(range(0, 101)), 101))
