# Graph & Digraph
import random


class Node(object):
    def __init__(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def __str__(self):
        return self.name


class Edge(object):
    def __init__(self, source, destination, weight=0):
        self.source = source
        self.destination = destination
        self.weight = weight

    def get_source(self):
        return self.source

    def get_destination(self):
        return self.destination

    def get_weight(self):
        return self.weight

    def __str__(self):
        return '{}->{}'.format(self.source, self.destination)


class Digraph(object):
    def __init__(self):
        self.nodes = set([])
        self.edges = {}

    def add_node(self, node):
        if node.get_name() in self.nodes:
            raise ValueError('Duplicate node')
        else:
            self.nodes.add(node)
            self.edges[node] = []

    def add_edge(self, edge):
        src = edge.get_source()
        dest = edge.get_destination()
        if not (src in self.nodes and dest in self.nodes):
            raise ValueError('Node not in graph')
        self.edges[src].append(dest)

    def children_of(self, node):
        return self.edges[node]

    def has_node(self, node):
        return node in self.nodes

    def __str__(self):
        res = ''
        for k in self.edges:
            for d in self.edges[k]:
                res = res + str(k) + '->' + str(d) + '\n'
        return res[:-1]


class Graph(Digraph):
    def add_edge(self, edge):
        Digraph.add_edge(self, edge)
        reverse_edge = Edge(edge.get_destination(), edge.get_source())
        Digraph.add_edge(self, reverse_edge)


# Build a graph/digraph
def test1(kind):
    nodes = []
    for name in range(6):
        nodes.append(Node(str(name)))

    g = kind()
    for n in nodes:
        g.add_node(n)
    g.add_edge(Edge(nodes[0], nodes[1]))
    g.add_edge(Edge(nodes[1], nodes[2]))
    g.add_edge(Edge(nodes[2], nodes[3]))
    g.add_edge(Edge(nodes[3], nodes[4]))
    g.add_edge(Edge(nodes[3], nodes[5]))
    g.add_edge(Edge(nodes[0], nodes[2]))
    g.add_edge(Edge(nodes[1], nodes[0]))
    g.add_edge(Edge(nodes[4], nodes[0]))
    print('Graph:', kind.__name__)
    print(g)


# test1(Digraph)
# test1(Graph)


# Shortest path (Depth-First Search): simple
# Explores as far as possible along each branch before backtracking
def shortest_path_dfs(graph, start, end, to_print=False, visited=[]):
    global num_calls  # big_test2 only
    num_calls += 1  # big_test2  only

    if to_print:
        print('Start: {}, End: {}'.format(start, end))
    if not (graph.has_node(start) and graph.has_node(end)):
        raise ValueError('Start or end not in graph')
    path = [str(start)]
    if start == end:
        return path

    shortest = None
    for node in graph.children_of(start):
        if str(node) not in visited:  # avoid cycles
            visited = visited + [str(node)]  # new list
            new_path = shortest_path_dfs(graph, node, end, to_print, visited)
            if new_path is None:
                continue
            if shortest is None or len(new_path) < len(shortest):
                shortest = new_path
    if shortest is not None:
        path = path + shortest
    else:
        path = None
    return path


# Find a shortest path (DFS) on a small graph
def test2(kind, to_print=False):
    nodes = []
    for name in range(6):
        nodes.append(Node(str(name)))

    g = kind()
    for n in nodes:
        g.add_node(n)
    g.add_edge(Edge(nodes[0], nodes[1]))
    g.add_edge(Edge(nodes[1], nodes[2]))
    g.add_edge(Edge(nodes[2], nodes[3]))
    g.add_edge(Edge(nodes[3], nodes[4]))
    g.add_edge(Edge(nodes[3], nodes[5]))
    g.add_edge(Edge(nodes[0], nodes[2]))
    g.add_edge(Edge(nodes[1], nodes[0]))
    g.add_edge(Edge(nodes[4], nodes[0]))

    print('Graph:', kind.__name__)
    print(g)
    shortest = shortest_path_dfs(g, nodes[0], nodes[4], to_print)
    print('The shortest path:', shortest)


# test2(Digraph)
# test2(Graph)


# Find a shortest path (DFS) on a large random graph
def big_test1(kind, num_nodes=25, num_edges=200):
    nodes = []
    for name in range(num_nodes):
        nodes.append(Node(str(name)))

    g = kind()
    for n in nodes:
        g.add_node(n)
    for e in range(num_edges):
        src = nodes[random.choice(range(len(nodes)))]
        dest = nodes[random.choice(range(len(nodes)))]
        g.add_edge(Edge(src, dest))

    print('Graph:', kind.__name__)
    print(g)
    shortest = shortest_path_dfs(g, nodes[0], nodes[4])
    print('The shortest path:', shortest)


# big_test1(Digraph)
# test2(Graph, to_print=True)


# Shortest path (DFS): dynamic programming
def shortest_path_dfs_dp(graph, start, end, visited=[], memo={}):
    global num_calls  # big_test2 only
    num_calls += 1  # big_test2  only

    if not (graph.has_node(start) and graph.has_node(end)):
        raise ValueError('Start or end not in graph')
    path = [str(start)]
    if start == end:
        return path
    shortest = None

    for node in graph.children_of(start):
        if str(node) not in visited:
            visited = visited + [str(node)]
            try:
                new_path = memo[node, end]
            except KeyError:
                new_path = shortest_path_dfs_dp(graph, node, end, visited, memo)
            if new_path is None:
                continue
            if shortest is None or len(new_path) < len(shortest):
                shortest = new_path
                memo[node, end] = new_path

    if shortest is not None:
        path = path + shortest
    else:
        path = None
    return path


# Find a shortest path (DFS) on a small graph: dynamic & simple
def test3(kind):
    nodes = []
    for name in range(6):
        nodes.append(Node(str(name)))

    g = kind()
    for n in nodes:
        g.add_node(n)
    g.add_edge(Edge(nodes[0], nodes[1]))
    g.add_edge(Edge(nodes[1], nodes[2]))
    g.add_edge(Edge(nodes[2], nodes[3]))
    g.add_edge(Edge(nodes[3], nodes[4]))
    g.add_edge(Edge(nodes[3], nodes[5]))
    g.add_edge(Edge(nodes[0], nodes[2]))
    g.add_edge(Edge(nodes[1], nodes[0]))
    g.add_edge(Edge(nodes[4], nodes[0]))

    print('Graph:', kind.__name__)
    print(g)
    shortest = shortest_path_dfs(g, nodes[0], nodes[4])
    print('The shortest path:', shortest)
    dp_shortest = shortest_path_dfs_dp(g, nodes[0], nodes[4])
    print('The dynamic programming shortest path:', dp_shortest)


# test3(Digraph)


# Find a shortest path (DFS) on a large random graph: dynamic VS simple
def big_test2(kind, num_nodes=25, num_edges=200):
    global num_calls
    nodes = []
    for name in range(num_nodes):
        nodes.append(Node(str(name)))

    g = kind()
    for n in nodes:
        g.add_node(n)
    for e in range(num_edges):
        src = nodes[random.choice(range(0, len(nodes)))]
        dest = nodes[random.choice(range(0, len(nodes)))]
        g.add_edge(Edge(src, dest))

    print('Graph:', kind.__name__)
    print(g)

    num_calls = 0
    shortest = shortest_path_dfs(g, nodes[0], nodes[4])
    print('The shortest path:', shortest)
    print('Number of calls to shortest path =', num_calls)

    num_calls = 0
    dp_shortest = shortest_path_dfs_dp(g, nodes[0], nodes[4])
    print('The dynamic programming shortest path:', dp_shortest)
    print('Number of calls to dp shortest path =', num_calls)


# big_test2(Digraph)


# Shortest path (Breadth-first search)
# Explores all neighbor nodes at present depth prior to moving to next depth level
def shortest_path_bfs(graph, paths, end, to_print=False):
    """
    paths is a list of partial path tuples, in the form of
    (NODELIST, LENGTH) where
    NODELIST is a sequential list of nodes in the path and
    LENGTH is an int denoting the number of nodes in the path
    """
    (NODELIST, LENGTH) = (0, 1)
    paths = sorted(paths, key=lambda path: path[LENGTH])

    if to_print:
        print('Path list so far:')
        for path in paths:
            node_names = [node.get_name() for node in path[NODELIST]]
            print((node_names, path[LENGTH]))

    new_paths = []
    examining_path = paths.pop(0)

    for node in graph.children_of(examining_path[NODELIST][-1]):
        if node == end:
            shortest = examining_path[NODELIST] + [node]
            return [node.get_name() for node in shortest]

        if node not in examining_path[NODELIST]:
            new_paths.append((examining_path[NODELIST] + [node], examining_path[LENGTH] + 1))

    paths.extend(new_paths)
    return shortest_path_bfs(graph, paths, end, to_print)


# Find a shortest path (BFS) on a small graph
def test4(kind, to_print=False):
    nodes = []
    for name in range(6):
        nodes.append(Node(str(name)))

    g = kind()
    for n in nodes:
        g.add_node(n)
    g.add_edge(Edge(nodes[0], nodes[1]))
    g.add_edge(Edge(nodes[1], nodes[2]))
    g.add_edge(Edge(nodes[2], nodes[3]))
    g.add_edge(Edge(nodes[3], nodes[4]))
    g.add_edge(Edge(nodes[3], nodes[5]))
    g.add_edge(Edge(nodes[0], nodes[2]))
    g.add_edge(Edge(nodes[1], nodes[0]))
    g.add_edge(Edge(nodes[4], nodes[0]))

    print('Graph:', kind.__name__)
    print(g)
    shortest = shortest_path_bfs(g, [([nodes[0]], 1)], nodes[4], to_print)
    print('The shortest path:', shortest)


test4(Digraph, to_print=True)
test4(Graph, to_print=True)
